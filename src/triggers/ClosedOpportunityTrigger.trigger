trigger ClosedOpportunityTrigger on Opportunity (after insert, after update) {
    List<Opportunity> oppList = new List<Opportunity>([select Id from Opportunity where Id in:Trigger.New and StageName='Closed Won']);
    
    List<Task> taskList = new List<Task>();
    for(Opportunity o : Trigger.New) {
        taskList.add(new Task(Subject = 'Follow Up Test Task',
                             WhatId = o.Id));
    }
    
    insert taskList;
}