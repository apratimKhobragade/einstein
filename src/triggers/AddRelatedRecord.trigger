trigger AddRelatedRecord on Account (after insert, after update) {
    Map<Id,Account> accountList = new Map<Id,Account>([select Id,(select Id from Opportunities) from Account where Id in:Trigger.New]);
    
    List<Opportunity> oppList = new List<Opportunity>();
    
    for(Account a : Trigger.New) {
        if(accountList.get(a.Id).Opportunities.size()==0) {
           oppList.add(new Opportunity(Name = a.Name+' Opportunity', CloseDate = System.today().addMonths(1),StageName = 'Prospecting',AccountId=a.Id));
        }
    }
    if(oppList.size()>0) {
        insert oppList; 
    }
    
    /*List<Opportunity> oppList = new List<Opportunity>();
    
    for(Account a : [SELECT Id,Name FROM Account
                     WHERE Id IN :Trigger.New AND
                     Id NOT IN (SELECT AccountId FROM Opportunity)]) {
                         oppList.add(new Opportunity(Name = a.Name+' Opportunity', CloseDate = System.today().addMonths(1),StageName = 'Prospecting',AccountId=a.Id));
                     }
    
    if(oppList.size()>0) {
        insert oppList;
    }*/
}