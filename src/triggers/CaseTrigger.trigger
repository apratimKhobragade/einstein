trigger CaseTrigger on Case (after insert, after update) {

    if(Trigger.isAfter) {
        if(Trigger.isInsert) {
            List<Case_Assign_History__c> cahList = new List<Case_Assign_History__c>();
            for(Case c : Trigger.New) {
                Case_Assign_History__c cah = new Case_Assign_History__c();
                if(c.Assign_To__c != null) {
                    cah.Assigned_User__c = c.Assign_To__c;
                } else {
                    //assign to contact name
                    System.debug(c.Assign_To__c);
                    cah.Assigned_User__c = c.Assign_To__c;
                }
                cah.Status__c = c.Status;
                cah.Case__c = c.Id;
                cahList.add(cah);
            }
            insert cahList;
        }
        
        if(Trigger.isUpdate) {
            List<Case_Assign_History__c> cahList = new List<Case_Assign_History__c>();
            for(Case c : Trigger.New) {
                if(c.Assign_To__c != null && (Trigger.oldMap.get(c.Id).Assign_To__c != Trigger.newMap.get(c.Id).Assign_To__c)) {
                    Case_Assign_History__c cah = new Case_Assign_History__c();
                    cah.Assigned_User__c = c.Assign_To__c;
                    cah.Status__c = c.Status;
                    cah.Case__c = c.Id;
                    cahList.add(cah);
                }
            }
            insert cahList;
        }
    }
}