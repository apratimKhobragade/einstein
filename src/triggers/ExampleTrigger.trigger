trigger ExampleTrigger on Contact (after update) {
    
    if(Trigger.isAfter && Trigger.isUpdate) {
        List<Account> accounts = [SELECT Id,Name,AccountNumber FROM Account WHERE Id IN :Trigger.newMap.keySet()];
        for(Account a : accounts) {
            a.AccountNumber = String.valueOf(a.Id);
            System.debug('Updated: ' + a.Id);
        }
        update accounts;
    }
}