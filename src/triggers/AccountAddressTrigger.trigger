trigger AccountAddressTrigger on Account (after update) {
    
    if(Trigger.isAfter && Trigger.isUpdate) {
        Set<Id> aIds = Trigger.newMap.keySet();
        List<Contact> contacts = [SELECT Id,AccountId,LastName FROM Contact WHERE AccountId IN :aIDs];
        for(Contact c : contacts) {
            c.LastName = 'AccountId ' + c.AccountId;
            System.debug('Updated: ' + c.LastName);
        }
        update contacts;
    }
}