@isTest
public class TestFakeClass {
    
    @testSetup static void setup() {
        List<Account> accountList = new List<Account>();
        for(Integer i = 0; i < 5; i++) {
            Account a = new Account();
            a.Name = 'Account ' + i;
            accountList.add(a);
        }
        insert accountList;
    }
    
    @isTest static void testGetAllAccounts() {
        Test.startTest();
        System.assertEquals(FakeClass.getAllAccounts(), 4);
        Test.stopTest();
    }
}