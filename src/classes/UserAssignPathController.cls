public class UserAssignPathController {

    @AuraEnabled
    public static List<String> getAllAssignedUsers(String recordId) {
        List<String> assignedUsers = new List<String>();
        List<Case_Assign_History__c> c = [SELECT Id,Case__c,Assigned_User__c,CreatedDate FROM Case_Assign_History__c WHERE Case__c=:recordId ORDER BY CreatedDate ASC];
        for(Case_Assign_History__c cah : c) {
            assignedUsers.add(cah.Assigned_User__c);
        }
        return assignedUsers;
    }
    
    @AuraEnabled
    public static void addAssignedUser(String recordId, String user) {
        Case_Assign_History__c cah = new Case_Assign_History__c();
        cah.Case__c = recordId;
        cah.Assigned_User__c = user;
        try {
            insert cah;
        } catch(DmlException e) {
            throw new DmlException(e);
        }
    }
    
    @AuraEnabled
    public static List<String> getAllUser() {
        List<String> users = new List<String>();
        Schema.DescribeFieldResult fieldResult = Case.Assign_To__c.getDescribe();
        List<Schema.PicklistEntry> pList = fieldResult.getPicklistValues();
        System.debug(pList);
        for(Schema.PicklistEntry p : pList) {
            users.add(p.getLabel());
        }
        return users;
    }
}