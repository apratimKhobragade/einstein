public class AnimalLocator {
//comment
    public static String getAnimalNameById(Integer animalId) {
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('https://th-apex-http-callout.herokuapp.com/animals/'+animalId);
        request.setMethod('GET');
        HttpResponse response = http.send(request);
        
        String name = '';
        if(response.getStatusCode()==200) {
            Map<String,Object> results = (Map<String,Object>) JSON.deserializeUntyped(response.getBody());
            
            Map<String,Object> animal = (Map<String,Object>) results.get('animal');
            name = String.valueOf(animal.get('name'));
           	System.debug(name);
        }
        return name;
    }
}