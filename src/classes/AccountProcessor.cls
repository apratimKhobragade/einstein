public class AccountProcessor {
    @future
    public static void countContacts(List<Id> accountId) {
        List<Account> accounts = [select Id,Number_of_Contacts__c from Account where Id in:accountId];
        List<Account> updateAccount = new List<Account>();
        for(Account acc : accounts) {
            Id idToFind = acc.Id;
            List<Contact> contacts = [select Id from Contact where AccountId=:idToFind];
            acc.Number_of_Contacts__c = contacts.size();
            updateAccount.add(acc);
        }
        update updateAccount;
    }
}