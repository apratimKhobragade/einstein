global class DailyLeadProcessor implements Schedulable {
    global void execute(SchedulableContext ctx) {
        List<Lead> leads = [select Id,LeadSource from Lead where LeadSource=''];
        for(Lead lead : leads) {
            lead.LeadSource = 'Dreamforce';
        }
        update leads;
    }

}