public class AddPrimaryContact implements Queueable {
	//comments
    Contact contacts;
    String state;
    
    public AddPrimaryContact(Contact contacts, String state) {
        this.contacts = contacts;
        this.state = state;
    }
    
    public void execute(QueueableContext context) {
        List<Account> accounts = [select ID from Account where BillingState=:state limit 200];
        List<Contact> updateContact = new List<Contact>();
        for(Account account : accounts) {
            Contact cloneContact = new Contact();
            cloneContact = this.contacts.clone(false);
            cloneContact.AccountId = account.Id;
            updateContact.add(cloneContact);
        }
        insert updateContact;
    }
}