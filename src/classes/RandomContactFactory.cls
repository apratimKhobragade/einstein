public class RandomContactFactory {
    public static List<Contact> generateRandomContacts(Integer n, String lastName) {
        List<Contact> contactList = new List<Contact>();
        for(Integer i = 0; i < n; i++) {
            String firstName = 'Test' + i;
            Contact con = new Contact(FirstName=firstName,LastName=lastName);
            contactList.add(con);
        }
        return contactList;
    }
}