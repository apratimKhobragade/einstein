@isTest
public class AccountProcessorTest {
    @isTest static void TestAccountProcessor() {
        Account a = new Account();
        a.Name = 'Test Account';
        insert a;
        
        Contact c = new Contact();
        c.FirstName = 'Smith';
        c.LastName = 'John';
        c.AccountId = a.Id;
        insert c;
        
        List<Id> accountIds = new List<Id>();
        accountIds.add(a.Id);
        
        Test.startTest();
        AccountProcessor.countContacts(accountIds);
        Test.stopTest();
        
        Account acc = [select Number_of_Contacts__c from Account where Id=:a.Id limit 1];
        System.assertEquals(Integer.valueOf(acc.Number_of_Contacts__c), 1);
    }
}