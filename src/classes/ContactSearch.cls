public class ContactSearch {
    public static List<Contact> searchForContacts(String lastName, String postalCode) {
        Contact[] contacts = [select ID,Name from Contact where LastName=:lastName AND MailingPostalCode=:postalCode];
        return contacts;
    }
}