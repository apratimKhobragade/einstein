@isTest
public class TestVerifyDate {
    @isTest static void testVerifyDate1() {
        Date dateTest = VerifyDate.CheckDates(Date.newInstance(2019, 8, 7),Date.newInstance(2019, 8, 15));
        System.assertEquals(Date.newInstance(2019, 8, 15),dateTest);
    }
    
    @isTest static void testVerifyDate2() {
        Date dateTest = VerifyDate.CheckDates(Date.newInstance(2019, 8, 7),Date.newInstance(2019, 8, 1));
        System.assertEquals(Date.newInstance(2019, 8, 31),dateTest);
    }
}