@RestResource(urlMapping='/Accounts/*')
global with sharing class AccountManager {
	//comments
    @HttpGet
    global static Account getAccount() {
        RestRequest request = RestContext.request;
        String accountId = request.requestURI.substring(request.requestURI.indexOf('Accounts/')+9,request.requestURI.indexOf('/contacts'));
        Account account = [select ID,Name,(select ID,name from contacts) from Account where id=:accountId];
        return account;
    }
}