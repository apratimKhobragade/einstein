@isTest
public class TestAccountAddressTrigger {
    
    @testSetup static void setup() {
        Account a  = new Account();
        a.Name = 'Test Account';
        a.AccountNumber = '1234567';
        insert a;
        
        Contact c = new Contact();
        c.LastName = 'Test contact';
        c.AccountId = a.Id;
        insert c;
    }
    
    @isTest static void test() {
        Test.startTest();
        Account a = [SELECT Id,Name,AccountNumber FROM Account][0];
        a.Name = 'Update account';
        update a;
        Test.stopTest();
    }
}