public class MainClass {
    public static void getData(Map<String,String> inputMap) {
        
        Map<String,String> functionMap = FunctionFactory.functionMap();
        
        for(String k : inputMap.keySet()) {
            Callable extension = (Callable) Type.forName('FunctionFactory').newInstance();
            String configId = (String) extension.call(functionMap.get(k), new Map<String,Object> {'configId' => 'configId'});
            System.debug(configId);
        }
    } 
}