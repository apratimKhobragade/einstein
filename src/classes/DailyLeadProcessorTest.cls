@isTest
public class DailyLeadProcessorTest {
    public static String CRON_EXP = '0 0 0 15 3 ? 2022';
    static testmethod void dailyLeadProcessorTest() {
        List<Lead> leads = new List<Lead>();
        for(Integer i = 0; i < 200; i++) {
            Lead lead = new Lead(LastName = 'LastName '+i, Company = 'Web');
            leads.add(lead);
        }
        insert leads;
        
        Test.startTest();
        String jobId = System.schedule('DailyLeadProcessor', CRON_EXP, new DailyLeadProcessor());
        Test.stopTest();
        
        System.assertEquals(200,[select count() from Lead where LeadSource = 'Dreamforce']);
    }
}