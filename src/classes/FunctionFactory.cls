public class FunctionFactory {

    public static String unreserveNumbers(String configId) {
        return 'unreserveNumbers';
    } 
    
    public static String getReservedNumbers(String configId) {
        return 'getReservedNumbers';
    }
    
    public static String detachAccountFromSite(String configId) {
        return 'detachAccountFromSite';
    }
    
    public static Map<String,String> functionMap() {
        Map<String,String> functionMap = new Map<String,String>();
        functionMap.put('UnreserveNumbers','unreserveNumbers');
        functionMap.put('GetReservedNumbers','getReservedNumbers');
        functionMap.put('DetachAccountFromSite','detachAccountFromSite');
        return functionMap;
    }
}