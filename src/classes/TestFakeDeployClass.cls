@isTest
public class TestFakeDeployClass {
    
    @testSetup static void setup() {
        List<Account> accountList = new List<Account>();
        for(Integer i = 0; i < 15; i++) {
            Account a = new Account();
            a.Name = 'Test Account ' + i;
            accountList.add(a);
        }
        insert accountList;
    }

    @isTest static void testGetAllUserStories() {
        Test.startTest();
        System.assertEquals(FakeDeployClass.getAllUserStories(),15);
        Test.stopTest();
    }
}