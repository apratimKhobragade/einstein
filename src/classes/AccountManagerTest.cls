@isTest
private class AccountManagerTest {
    @isTest static void getAccountTest() {
        Id recordId = createTestRecord();
        
        RestRequest request = new RestRequest();    
        request.requestURI = 'https://ap15.salesforce.com/services/apexrest/Accounts/'
            + recordId + '/contacts';
        request.httpMethod = 'GET';
        RestContext.request = request;
        
        Account a = AccountManager.getAccount();
        System.assert(a!=null);
        System.assertEquals(recordId, a.Id);
    }
    
    static Id createTestRecord() {
        Account accTest = new Account(Name='Test');
        insert accTest;
        return accTest.Id;
    }
}